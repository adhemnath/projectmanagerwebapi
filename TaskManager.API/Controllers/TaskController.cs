﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManaager.Entities;
using TaskManager.BAL;

namespace TaskManager.API.Controllers
{
    [Route("api/Task")]
    public class TaskController : ApiController
    {
        [HttpGet]
        [Route("GetAllTasks")]
        public IHttpActionResult GetAllTasks()
        {
            TaskBusiness tb = new TaskBusiness();
            List<Task> allTasks = tb.GetAllTasks();
            return Ok(allTasks);
            //test
        }


        [HttpGet]
        [Route("GetTaskByID")]
        public IHttpActionResult GetTaskByID(string id)
        {
            TaskBusiness tb = new TaskBusiness();
            Task selectedTask = tb.GetTaskByID(id);


            UserBusiness userBusiness = new UserBusiness();
            List<User> allUsers = userBusiness.GetAllUsers();
            User user = allUsers.Where(A => A.Task_ID == selectedTask.Task_ID.Trim()).FirstOrDefault();
            
            TaskDetails taskDetails = new TaskDetails();
            taskDetails.Parent_ID = selectedTask.Parent_ID;
            taskDetails.Priority = selectedTask.Priority;
            taskDetails.StartDate = selectedTask.StartDate;
            taskDetails.EndDate = selectedTask.EndDate;
            taskDetails.Status = selectedTask.Status;

            if (user != null)
            {
                taskDetails.User_ID = user.User_ID;
            }
            taskDetails.TaskName = selectedTask.TaskName;
            taskDetails.Project_ID = selectedTask.Project_ID;
            taskDetails.Task_ID = selectedTask.Task_ID;

            //ParentTask.

            ProjectBusiness projectBusiness =  new ProjectBusiness();
            List<ParentTask> allParentProjects = projectBusiness.GetParentTasks();

            ParentTask parentTask = allParentProjects.Where(A => A.Parent_ID == selectedTask.Parent_ID).FirstOrDefault();
            if (parentTask != null)
            {
                taskDetails.Parent_Task = parentTask.Parent_Task;
            }



            return Ok(taskDetails);
        }


        [HttpPost]
        [Route("PostTask")]
        public IHttpActionResult PostTask([FromBody] TaskDetails TaskDetails)
        {           
            TaskBusiness tb = new TaskBusiness();
            Task taskToAdd = new Task();
            if (TaskDetails.IsParent_Task != "Y")
            {
                taskToAdd.EndDate = TaskDetails.EndDate;
                taskToAdd.StartDate = TaskDetails.StartDate;
                taskToAdd.EndDate = TaskDetails.EndDate;
                taskToAdd.Status = TaskDetails.Status;
            }
            taskToAdd.TaskName = TaskDetails.TaskName;
            taskToAdd.Task_ID = TaskDetails.Task_ID;            
                taskToAdd.Parent_ID = TaskDetails.Parent_ID;
            if (TaskDetails.IsParent_Task != "Y")
            {
                taskToAdd.Priority = TaskDetails.Priority;
                taskToAdd.Project_ID = TaskDetails.Project_ID;
            }

            taskToAdd.Task_ID = Guid.NewGuid().ToString();
            tb.AddTask(taskToAdd);

            //update User table
            UserBusiness userBusiness = new UserBusiness();
            List<User> allUsers = userBusiness.GetAllUsers();

            User user = allUsers.Where(A => A.User_ID == TaskDetails.User_ID).FirstOrDefault();
            if (user != null)
            {
                user.Project_ID = taskToAdd.Project_ID;
                user.Task_ID = taskToAdd.Task_ID;
                userBusiness.UpdateUser(user);
            }

            if (TaskDetails.IsParent_Task == "Y")
            {
                ProjectBusiness projectBusiness = new ProjectBusiness();
                ParentTask parentTask = new ParentTask();
                parentTask.Parent_ID = Guid.NewGuid().ToString();
                parentTask.Parent_Task = TaskDetails.TaskName;
                projectBusiness.AddParentTask(parentTask);
            }
            return Ok(taskToAdd);
        }

        [HttpPut]
        [Route("UpdateTask")]
        public IHttpActionResult UpdateTask([FromBody] TaskDetails TaskDetails)
        {
            TaskBusiness tb = new TaskBusiness();            
            Task taskToUpdate = new Task();
            if (TaskDetails.IsParent_Task != "Y")
            {
                taskToUpdate.EndDate = TaskDetails.EndDate;
                taskToUpdate.StartDate = TaskDetails.StartDate;
                //taskToUpdate.EndDate = TaskDetails.EndDate;
                taskToUpdate.Status = TaskDetails.Status;
            }
            taskToUpdate.TaskName = TaskDetails.TaskName;
            taskToUpdate.Task_ID = TaskDetails.Task_ID;
            taskToUpdate.Parent_ID = TaskDetails.Parent_ID;
            if (TaskDetails.IsParent_Task != "Y")
            {
                taskToUpdate.Priority = TaskDetails.Priority;
                taskToUpdate.Project_ID = TaskDetails.Project_ID;
            }
            taskToUpdate.Status = TaskDetails.Status;
            tb.UpdateTask(taskToUpdate);

            //Update user table
            UserBusiness userBusiness = new UserBusiness();
            List<User> allUsers = userBusiness.GetAllUsers();

            User user = allUsers.Where(A => A.User_ID == TaskDetails.User_ID).FirstOrDefault();
            if (user != null)
            {
                user.Project_ID = taskToUpdate.Project_ID;
                user.Task_ID = taskToUpdate.Task_ID;
                userBusiness.UpdateUser(user);
            }

            if (TaskDetails.IsParent_Task == "Y")
            {
                ProjectBusiness projectBusiness = new ProjectBusiness();
                ParentTask parentTask = new ParentTask();
                parentTask.Parent_ID = Guid.NewGuid().ToString();
                parentTask.Parent_Task = taskToUpdate.TaskName;
                projectBusiness.AddParentTask(parentTask);
            }
            return Ok(taskToUpdate);
        }


        /*[HttpDelete]
        [Route("DeleteTask")]
        public IHttpActionResult DeleteTask(string id)
        {
            TaskBusiness tb = new TaskBusiness();
            var isDeleted = tb.DeleteTask(id);
            return Ok(isDeleted);
        }*/

        //[HttpOptions]
        //public HttpResponseMessage Options()
        //{
        //    var responesMsg = new HttpResponseMessage();
        //    responesMsg.StatusCode = HttpStatusCode.OK;
        //    return responesMsg;

        //}
    }
}
