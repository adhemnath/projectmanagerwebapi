﻿using NBench;
using TaskManager.API.Controllers;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManager.BAL;

namespace TaskManager.Performance.Test
{
    public class TaskPerformanceTest
    {
        [PerfBenchmark(Description = "Performace Test for GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 500000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void GetAllTasks_PassingTest()
        {
            TaskController controller = new TaskController();
            controller.GetAllTasks();
        }

        [PerfBenchmark(Description = "Performace Test for GET (By ID)", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 500000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void GetTaskByID_PassingTest()
        {
            TaskController tc = new TaskController();
            tc.GetTaskByID("44179644-dd7d-4928-be40-c6d816bb64fa");
        }

        [PerfBenchmark(Description = "Performace Test for POST", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 200000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void PostTask_PassingTest()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task()
            {
                TaskName = "Task from Pefromance Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2)
            };

            TaskDetails taskDetails = new TaskDetails()
            {
                TaskName = "Task from Pefromance Test Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2)
            };
            tc.PostTask(taskDetails);

        }
    }
}
