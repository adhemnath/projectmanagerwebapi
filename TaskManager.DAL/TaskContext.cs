﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using TaskManaager.Entities;

namespace TaskManager.DAL
{
    public class TaskContext : DbContext
    {
        public TaskContext() : base("name=TaskConnect")
        {
        }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }        
        public DbSet<User> Users { get; set; }
        public DbSet<ParentTask> ParentTasks { get; set; }

    }
}
