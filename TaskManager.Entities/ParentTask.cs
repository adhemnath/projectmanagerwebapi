﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManaager.Entities
{
    [Table("ParentTask")]
    public class ParentTask
    {
        [Key]
        public string Parent_ID { get; set; }
        public string Parent_Task { get; set; }

    }
}
