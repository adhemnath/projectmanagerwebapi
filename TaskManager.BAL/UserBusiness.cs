﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManaager.Entities;
using TaskManager.DAL;

namespace TaskManager.BAL
{
    public class UserBusiness
    {
        public List<User> GetAllUsers()
        {
            using (TaskContext _db = new TaskContext())
            {
                return _db.Users.ToList();
            }
        }

        public User GetUserByID(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Users.Any(a => a.User_ID == id))
                {
                    return _db.Users.FirstOrDefault(a => a.User_ID == id);
                }
                else
                {
                    throw new Exception(string.Format("User with the id - {0} could not be found", id));
                }
            }
        }

        public User GetUserByProjectID(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Users.Any(a => a.Project_ID == id))
                {
                    return _db.Users.LastOrDefault(a => a.Project_ID == id);
                }
                else
                {
                    return null;
                }
            }
        }

        public bool AddUser(User newUser)
        {
            using (TaskContext _db = new TaskContext())
            {
                _db.Users.Add(newUser);
                _db.SaveChanges();
                return true;
            }
        }

        public bool UpdateUser(User updateUser)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Users.Any(a => a.User_ID == updateUser.User_ID))
                {
                    User userToRemove = _db.Users.FirstOrDefault(a => a.User_ID == updateUser.User_ID);
                    _db.Users.Remove(userToRemove);
                    _db.Users.Add(updateUser);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("User with the name - {0} could not be found", updateUser.User_ID));
                }
            }
        }

        public bool DeleteUser(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Users.Any(a => a.User_ID == id))
                {
                    User userToRemove = _db.Users.FirstOrDefault(a => a.User_ID == id);
                    _db.Users.Remove(userToRemove);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("User with the name - {0} could not be found", id));
                }
            }
        }
    }
}
